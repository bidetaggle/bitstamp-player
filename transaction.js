/*
    convert an old pair data to a Transaction object
example:
-- input --
[
    {
        currency1: "btc",
        amount: 0.00196828,
        currency2: "usd",
        value: 10,
        operation: "buy",
        operationFee: 0.03,
        price: 5065.32,
        lastTicker: 0
    },
    {
        currency1: "xrp",
        amount: 30.45062976,
        currency2: "btc",
        value: 0.00196828,
        operation: "buy",
        operationFee: 0.00001,
        price: 0.00006431,
        lastTicker: 0
    },
    {
        currency1: "xrp",
        amount: 30.45062976,
        currency2: "usd",
        value: 9.85800271,
        operation: "sell",
        operationFee: 0.08,
        price: 0.32459,
        lastTicker: 0
    }
]

-- output --
[
    {
        pair: "btc/usd",
        type: "buy",
        price: 5065.32,
        from: {
            currency: "usd",
            value: 10,
            fee: 0.03,
            balanceAvailability: null,
        },
        to: {
            currency: "btc",
            value: 0.00196828
        },
        api: null,
        error: null
    },
    {
        pair: "xrp/btc",
        type: "buy",
        price: 0.00006431,
        from: {
            currency: "btc",
            value: 0.00196828,
            fee: 0.00001,
            balanceAvailability: null,
        },
        to: {
            currency: "xrp",
            value: 30.45062976
        },
        api: null,
        error: null
    },
    {
        pair: "xrp/usd",
        type: "sell",
        price: 0.32459,
        from: {
            currency: "xrp",
            value: 30.45062976,
            fee: 0.08,
            balanceAvailability: null,
        },
        to: {
            currency: "usd",
            value: 9.85800271
        },
        api: null,
        error: null
    }
]
*/

/*
{
    pair: null,
    operation: null,
    price: null,
    from: {
        currency: null,
        value: null,
        fee: null,
        balanceAvailability: null
    },
    to: {
        currency: null,
        value: null
    },
    api: null,
    error: null
}
*/

module.exports = function(pair) {
    this.pair = `${pair.currency1}/${pair.currency2}`;
    this.operation = pair.operation;
    this.price = pair.price;
    this.from = {
        currency: null,
        value: null,
        fee: pair.operationFee,
        balanceAvailability: null
    };
    this.to = {
        currency: null,
        value: null
    };
    this.api = null;

    if(pair.operation == 'buy'){
        this.from.currency = pair.currency2;
        this.from.value = pair.value;
        this.to.currency = pair.currency1;
        this.to.value = pair.amount;
    }
    else if(pair.operation == 'sell'){
        this.from.currency = pair.currency1;
        this.from.value = pair.amount;
        this.to.currency = pair.currency2;
        this.to.value = pair.value;
    }
    else {
        this.error = "INTERNAL ERROR: operation has to be either 'buy' or 'sell', its current value is: "+pair.operation;
    }

};
