const Bitstamp = require('bitstamp');

module.exports = function(apiKey, apiSecretKey, customerID){
    return new Bitstamp(apiKey, apiSecretKey, customerID, 10000, 'www.bitstamp.net');
};
