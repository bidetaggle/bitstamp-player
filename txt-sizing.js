module.exports = (input, size) => {
    let output = input;
    for(let i=0 ; i<size - input.toString().length ; i++)
        output += " ";

    return output;
};
