const txtSizing = require('./txt-sizing');
const colors = require('colors');

module.exports = (report) => {
    let finalLog = "";

    if(report.Funds){
        finalLog += "funds availability:\n";
        report.Funds.map((pair, i) => {
            let log = "";
            log += `${txtSizing(` ${i}`, 3)}-> `;
            log += `[${pair.currencyCode}] `;
            log += `${txtSizing(`need: ${pair.need}`, 20)}`;
            log += `${txtSizing(`available: ${pair.available}`, 25)}`;
            if(pair.success)
                log += `${txtSizing(`Success`, 26).green} `;
            else
                log += `${txtSizing(`${pair.error}`, 26).red} `;
            log += `${txtSizing(`(${pair.operation})`, 6)} `;

            log += "\n";

            finalLog += log;
        });
        finalLog += "\n";
    }

    if(report.Results){
        finalLog += "Sending orders... response:\n"
        report.Results.map((order, n) => {
            if(order.success){
                finalLog += `${txtSizing(` ${n}`, 3)}-> ${`Success`.green}: ${txtSizing(`${order.datetime}`, 29)}`;
                finalLog += txtSizing(`id: ${order.id}`, 17);
                finalLog += txtSizing(`API request attempts: ${order.apiRequestAttempts}\n`, 25);
                finalLog += ` ${order.currency1.toUpperCase()}/${order.currency2.toUpperCase()}  `;
                finalLog += txtSizing(order.operation, 5);
                finalLog += txtSizing('amount: '+order.amount, 21);
                finalLog += txtSizing('price: '+order.price, 19);
            }
            else if(!order.success){
                finalLog += `${txtSizing(` ${n}`, 3)}-> Err: ${order.error.red}`
            }
            finalLog += '\n';
        });
    }

    if(!report.Success)
        finalLog += `[FAIL]`.red + ` ${report.Error.yellow}`;

    return finalLog;
};
