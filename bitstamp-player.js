var PrivateBitstamp = require('./private-bitstamp');
const Transaction = require('./transaction');

module.exports = function(apiKey, apiSecretKey, customerID){
    /* Constructor */
    PrivateBitstamp = require('./private-bitstamp')(apiKey, apiSecretKey, customerID);

    const getBalances = () => new Promise((resolve, reject) => {
        PrivateBitstamp.balance(null, (req, res) => {
            if(res) resolve(res);
            else reject("Err: Balance download failed (is the internet connection ok?)");
        });
    });

    this.log = require('./log.js');
    this.getBalances = getBalances;

    const limitOrder = (transaction) => new Promise(function(resolve, reject){
        const Callback = (req,res) => {
            if(res){
                resolve(res);
            }
            else {
                reject(`${req.name} | message: ${req.message} | status: ${req.meta.status}, reason: ${req.meta.reason}, code: ${req.meta.code}`);
            }
        };

        if(transaction.operation == 'buy')
            PrivateBitstamp.buy(`${transaction.pair.split("/")[0]}${transaction.pair.split("/")[1]}`, transaction.to.value, transaction.price, null, Callback);
        else if(transaction.operation == 'sell')
            PrivateBitstamp.sell(`${transaction.pair.split("/")[0]}${transaction.pair.split("/")[1]}`, transaction.from.value, transaction.price, null, Callback);
    });

    const multipleLimitOrder = (transactions) => new Promise((feedback,fail) => {
        let response = {
            success: true,
            serverReply: []
        }
        let counter = 0;

        transactions.map((transaction,n) => {
            function makeTransaction(requestAttempts){
                response.serverReply[n] = {};
                response.serverReply[n].requestAttempts = requestAttempts;
                limitOrder(transaction)
                .then((res) => {

                    response.serverReply[n].response = res;

                    counter++;
                    if(counter == transactions.length){
                        feedback(response);
                    }
                    else if(counter > transactions.length)
                        fail(`OMG this problem is not supposed to come`);
                })
                .catch((err) => {
                    if(requestAttempts < 4)
                        makeTransaction(++requestAttempts);
                    else{
                        response.serverReply[n] = {
                            error: `Too many attempts (x4) failed! Last error: ${err}`
                        };

                        counter++;
                    }

                    if(counter == transactions.length){
                        feedback(response);
                    }
                    else if(counter > transactions.length)
                        fail(`OMG this problem is not supposed to come`);
                });
            };
            makeTransaction(1);
        });
    });

    this.executeTransactionsCluster = (transactions) => {
        return new Promise((resolve, reject) => {
            getBalances().then((balanceResult) => {
                /*
                * check funds availability first
                */
                let i = 0;
                for(var n=0 ; n<transactions.length ; n++){
                    transactions[n].from.balanceAvailability = eval(`balanceResult.${transactions[n].from.currency}_available`);
                    if(transactions[n].from.value < transactions[n].from.balanceAvailability){
                        transactions[n].error = false;
                        i++;
                    }
                    else {
                        transactions[n].error = "Insufficient funds.";
                    }
                }

                let circlePosition = {
                    error: null,
                    transactions: transactions
                };

                //console.log(`middle code:\n${JSON.stringify(circlePosition, null, 4)}`);

                if(i == transactions.length){
                //if(true){ //DEBUG line
                    multipleLimitOrder(transactions)
                    .then((result) => {
                        for(i in circlePosition.transactions) {
                            circlePosition.transactions[i].api = result.serverReply[i];
                            if(circlePosition.transactions[i].api.response.status == "error"){
                                circlePosition.error = "At least one order got a problem, check transactions[x].api.response.reason.__all__ for more details about it";
                                resolve(circlePosition);
                            }
                        }
                        resolve(circlePosition);
                    })
                    .catch((err) => {resolve("Fatal error: " + err)});
                }
                else{
                    circlePosition.error = "Insufficient funds for at least one order.";
                    resolve(circlePosition);
                }

            }).catch((err)=>{
                reject(err);
            });
        });
    }
}
