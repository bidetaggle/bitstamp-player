const BitstampPlayer = require('./bitstamp-player.js');

transactions = [
    {
        "pair": "btc/usd",
        "operation": "buy",
        "price": 9880.9,
        "from": {
            "currency": "usd",
            "value": 20,
            "fee": 0.05,
            "balanceAvailability": "32.20"
        },
        "to": {
            "currency": "btc",
            "value": 0.00201904
        }
    },
    {
        "pair": "xrp/btc",
        "operation": "buy",
        "price": 0.00003192,
        "from": {
            "currency": "btc",
            "value": 0.00201904,
            "fee": 0.00001,
            "balanceAvailability": "0.00210000"
        },
        "to": {
            "currency": "xrp",
            "value": 62.93984962
        }
    },
    {
        "pair": "xrp/usd",
        "operation": "sell",
        "price": 0.31454,
        "from": {
            "currency": "xrp",
            "value": 62.93984962,
            "fee": 0.16,
            "balanceAvailability": "63.00000000"
        },
        "to": {
            "currency": "usd",
            "value": 19.74677389
        }
    }
];

let BsPlayer = new BitstampPlayer('','',123456);

BsPlayer.executeTransactionsCluster(transactions)
.then(function(result){
    console.log(JSON.stringify(result, null, 4));
})
.catch(function(error){
    console.log(error);
});
