# Bitstamp-player #

Bitstamp-player introduce another way of seeing forex transactions with from a currency/value to another currency/value

- Send multiple orders (buy or sell) on bitstamp.net market at the same time.
- Check the user balance for each of its before sending the order requests
- If at least one order is not able to be sent because the balance, it doesn't send any order
- It happens that the communication with bitstamp API fail randomly due to a Nonce error, if it's the case, the program send again and again until it gets the expected callback (API requests attempts).

Check example.js to see the input data structure and usage

Copy example.js under the name of example.local.js, add your bitstamp credentials and try it with `node example.local.js`

## Development ##

### installation and running example ###

- `$ npm install`
- `$ cp example.js example.local.js`
- replace apiKey, apiSecretKey and customerID in `example.local.js`
- `$ npm start`
