var expect = require('chai').expect;
var txtSizing = require('../txt-sizing');

describe('txtSizing()', function(){
    it('should resize text string into a 10 character string', function(){
        // ARRANGE
        var txt = "test";
        var size = 10;

        // ACT
        var string = txtSizing(txt, size);

        // ASSERT
        expect(string).to.have.lengthOf(size);
    });
    it('should resize text string into a 100 character string', function(){
        // ARRANGE
        var txt = "test";
        var size = 100;

        // ACT
        var string = txtSizing(txt, size);

        // ASSERT
        expect(string).to.have.lengthOf(size);
    });
});
